#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  pred1.py
#  
#  Copyright 2018 Daniele Raimondi <eddiewrc@vega>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import matplotlib.pyplot as plt
import os, copy, random, time
from sys import stdout
import sources.utils as U
import numpy as np
import torch as t
t.set_num_threads(4)
from torch.autograd import Variable
from torch.nn.utils.rnn import PackedSequence, pack_padded_sequence, pad_packed_sequence
from torch.utils.data import Dataset, DataLoader
import sources.utilsSCRATCH as US
from scipy.stats import pearsonr


def pack_sequence(sequences,batch_first=True):
	return t.nn.utils.rnn.pack_padded_sequence(pad_sequence(sequences,batch_first=batch_first), [v.size(0) for v in sequences],batch_first=batch_first)

def pad_sequence(sequences, batch_first=True):
    max_size = sequences[0].size()
    max_len, trailing_dims = max_size[0], max_size[1:]
    prev_l = max_len
    if batch_first:
        out_dims = (len(sequences), max_len) + trailing_dims
    else:
        out_dims = (max_len, len(sequences)) + trailing_dims
    out_variable = sequences[0].new(*out_dims).zero_()
    lengths=[]
    for i, variable in enumerate(sequences): 
		length = len(sequences[i])         
		lengths.append(length)
		# temporary sort check, can be removed when we handle sorting internally
		if prev_l < length:
			raise ValueError("lengths array has to be sorted in decreasing order")
		prev_l = length
		# use index notation to prevent duplicate references to the variable
		if batch_first:
			out_variable[i, :length, ...] = variable
		else:
			out_variable[:length, i, ...] = variable
    return out_variable, lengths

def pad_VariableList(sequences, dsMax = None, batch_first=True):
	#print len(sequences)
	max_size = sequences[0].size()
	if dsMax != None:
		max_len = dsMax
	else:
		max_len = max_size[0]
	trailing_dims = [int(l) for l in list(max_size[1:])]
	#print trailing_dims
	lengths = []
	seqs = []
	for i, var in enumerate(sequences):
		length = len(sequences[i])
		lengths.append(length)
		if length == max_len:
			seqs.append(var)
			continue		
		missingDims =  int(max_len-var.size()[0])
		tmp = t.cat([var,Variable(t.zeros([missingDims]+ trailing_dims))])
		#print tmp.size()
		assert tmp.size()[0] == max_len
		seqs.append(tmp)
		#print tmp.size()
	seqs = t.stack(seqs,0)
	assert len(seqs) == len(lengths)
	return seqs, lengths

def sortForPadding(X, Y1, Y2):
	assert len(X) == len(Y1) == len(Y2)
	tmp = []
	i = 0
	while i < len(X):
		tmp.append((X[i], Y1[i], Y2[i], i))
		i+=1
	tmp = sorted(tmp, key=lambda x: len(x[0]), reverse=True)
	i = 0
	X = []
	Y1 = []
	Y2 = []
	order = []
	while i < len(tmp):
		X.append(t.Tensor(tmp[i][0]))
		Y1.append(t.Tensor(tmp[i][1]))
		Y2.append(t.Tensor(tmp[i][2]))
		order.append(tmp[i][-1])
		i+=1
	return X, Y1, Y2, order #lists of tensors

def unpad(x, l):
	preds=[]
	for i in xrange(len(x)):
		preds.append(x[i][:l[i]])
	return preds
	
def reconstruct(x, l):
	preds=[]
	pos = 0
	#print x.size()
	for p in l:
		tmp = x[pos:pos+p]
		#print tmp.size()
		preds.append(tmp)
		pos = p
	return preds	

class myDataset(Dataset):
    
	def __init__(self, X, Y = None):
		if Y == None:
			Y = []
			for x in X:
				Y.append(t.FloatTensor([0]))	
		else:
			tmp = []
			for y in Y:
				tmp.append(t.FloatTensor([y]))		
			Y = tmp
		self.Y = Y
		self.X = []
		for x in X:
			self.X.append(t.Tensor(x).transpose(0,1))
		assert len(self.Y) == len(self.X)	
		assert len(self.X) > 0

		
	def __len__(self):
		return len(self.X)

	def __getitem__(self, idx):
		return self.X[idx], self.Y[idx]

'''
x--> pad_sequence --> tensore paddato
tensore paddato --> pack_padded_sequence --> tensore paccato
tensore paccato --> pad_packed_sequence -->tensore paddato
tensore paddato --> unpad --> x
'''	

class secretionNN(t.nn.Module):
	
	def __init__(self, name = "secretedNN", preModel = None):
		super(secretionNN, self).__init__()
		os.system("mkdir -p models")
		self.name = name
		##########DEFINE NN HERE##############
		
		#self.hr1 = t.nn.Sequential(t.nn.LSTM(24, 10, 1, batch_first=True, bidirectional = False))
		self.c1 = t.nn.Sequential(t.nn.Conv1d(in_channels=20, out_channels=10, kernel_size=3 ,padding=1, stride=1),t.nn.BatchNorm1d(10), t.nn.ReLU(), t.nn.AdaptiveMaxPool1d(100),\
									t.nn.Conv1d(in_channels=10, out_channels=15, kernel_size=3 ,padding=1, stride=1), t.nn.BatchNorm1d(15), t.nn.ReLU(), t.nn.AdaptiveAvgPool1d(50),\
									t.nn.Conv1d(in_channels=15, out_channels=25, kernel_size=3 ,padding=1, stride=1), t.nn.BatchNorm1d(25), t.nn.ReLU(), t.nn.AdaptiveMaxPool1d(25),\
									t.nn.Conv1d(in_channels=25, out_channels=20, kernel_size=3 ,padding=1, stride=1), t.nn.BatchNorm1d(20), t.nn.ReLU(), t.nn.AdaptiveAvgPool1d(10))
		self.sec = t.nn.Sequential(t.nn.Linear(200,1), t.nn.Sigmoid())
		######################################
		#self.getNumParams()
	
	def forward(self, x):
		#print x.size()	
		o = self.c1(x).view(1,200)
		o = self.sec(o)
		return o
		
	def last_timestep(self, unpacked, lengths):
		# Index of the last output for each sequence.
		idx = (lengths - 1).view(-1, 1).expand(unpacked.size(0), unpacked.size(2)).unsqueeze(1)
		return unpacked.gather(1, idx).squeeze()
	
	def getWeights(self):
		return self.state_dict()
		
	def getNumParams(self):
		p=[]
		for i in self.parameters():
			p+= list(i.data.cpu().numpy().flat)
		print 'Number of parameters=',len(p)	


class NNwrapper():

	def __init__(self, model, initialEpoch=0):
		self.model = model
		self.initialEpoch = initialEpoch +1
	
	def fit(self, originalX, originalY, epochs = 100, batch_size=1, save_model_every=10, LOG=True):
		if LOG:
			os.system("rm -rf ./logs")
			os.system("killall tensorboard")
			os.system("tensorboard --logdir='./logs' --port=6006 &")
			from pytorchUtils.logger import Logger, to_np, to_var
			logger = Logger('./logs')			
			os.system("firefox http://arcturus:6006  &")
		batch_size = 1
		########DATASET###########
		dataset = myDataset(originalX, originalY)
		
		#######MODEL##############		
		parameters = list(self.model.sec.parameters())+\
					list(self.model.c1.parameters())
		p = []
		for i in parameters:
			p+= list(i.data.numpy().flat)
		print 'Number of parameters=',len(p)
		p = None	
		print "Start training"
		########LOSS FUNCTION######
		loss_fn1 = t.nn.BCELoss(size_average=False)
		
		########OPTIMIZER##########	
		self.learning_rate = 1e-2		
		optimizer = t.optim.Adam(parameters, lr=self.learning_rate, weight_decay=10)
		scheduler = t.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=0.5, patience=1, verbose=True, threshold=0.0001, threshold_mode='rel', cooldown=0, min_lr=0, eps=1e-08)
		
		########DATALOADER#########
		loader = DataLoader(dataset, batch_size=1, shuffle=False, sampler=None, num_workers=0)
		e = self.initialEpoch
		minLoss = 1000000000
		ofp = open("models/"+self.model.name+".TrainLog","w",0)		
		while e < epochs:			
			errTot = 0
			i = 1
			start = time.time()
			optimizer.zero_grad()
			for sample in loader:				
				x, y1 = sample
				#print "new sample: ", x.size()
				#print y.size()
				yp = self.model.forward(Variable(x)).view(1,1)	
				loss1 = loss_fn1(yp, Variable(y1))	
				errTot += loss1.data					
				loss1.backward()
				#print i	
				if i % 100 == 0:
					optimizer.step()					
				i += batch_size								
			end = time.time()						
			print " epoch %d, ERRORTOT: %f (%fs)" % (e,errTot, end-start)
			if LOG:
				loss = errTot
				net = self.model
				step = e
				#============ TensorBoard logging ============#
				# (1) Log the scalar values
				info = {'loss': loss}

				for tag, value in info.items():
					logger.scalar_summary(tag, value, step+1)

				# (2) Log values and gradients of the parameters (histogram)
				for tag, value in net.named_parameters():
					tag = tag.replace('.', '/')
					logger.histo_summary(tag, to_np(value), step+1)
					logger.histo_summary(tag+'/grad', to_np(value.grad), step+1)

				# (3) Log the images
				#info = {'images': to_np(images.view(-1, 28, 28)[:10])}

				#for tag, images in info.items():
				#	logger.image_summary(tag, images, step+1)
				# =============================================== #
			scheduler.step(float(errTot))
			if e % save_model_every == 0:
				print "Store model ", e
				t.save(self.model, "models/"+self.model.name+".iter_"+str(e)+".t")				
			stdout.flush()
										
			e += 1	
		t.save(self.model, "models/"+self.model.name+".final.t")
		ofp.close()		
	
	def predict(self, X, Y=None, batch_size=1, plotGraph=False):
		if plotGraph:
			from pytorchUtils.torchgraphviz1 import make_dot, make_dot_from_trace
		batch_size = 1
		print "Predicting..."
		dataset = myDataset(X, Y)
		loader = DataLoader(dataset, batch_size=batch_size, shuffle=False, sampler=None, num_workers=0)
		yp = []	
		first = True
		for sample in loader:
			x, y1 = sample	
			pred = self.model.forward(Variable(x))
			if first and plotGraph:
				first = False
				print "printing"
				#print dict(self.model.named_parameters())
				#raw_input()
				make_dot(pred.mean(), params=dict(self.model.named_parameters()))		
			yp.append(pred.data.squeeze().tolist()[0])			
		return yp

def mainPRE():
	db = U.readDB("datasets/test.csv")#db = {uid:(seq,label)}
	
	#train = U.retrieveUID(["datasets/cvsets5/set0","datasets/cvsets5/set1","datasets/cvsets5/set2","datasets/cvsets5/set3"])
	#test = U.retrieveUID(["datasets/cvsets5/set4"])
	#train = U.retrieveUID(["datasets/cvsets2/set0"])
	#test = U.retrieveUID(["datasets/cvsets2/set1"])
	train = db.keys()[:1500]
	test = db.keys()[1500:]
	X, Y = buildVectors(train, db)
	#TRAIN = False
	TRAIN = True
	if TRAIN:
		#model = t.load("models/secretionNN4.iter_40.t")
		model = secretionNN("secretionSimpleCnn")
		wrapper = NNwrapper(model, 0)
		wrapper.fit(X, Y, LOG = True)	
	else:
		model = t.load("models/secretionSimpleCnn.iter_20.t")
		wrapper = NNwrapper(model)
		
	print "Predict trainset..."
	Yp = wrapper.predict(X, plotGraph=False)
	U.getScoresSVR(Yp, Y, threshold=None, PRINT = True, CURVES = False, SAVEFIG=None)
	print "Predict test set..."
	x, y = buildVectors(test, db)
	yp = wrapper.predict(x)
	U.getScoresSVR(yp, y, threshold=None, PRINT = True, CURVES = False, SAVEFIG=None)

def printPreds(l, ids, seqs, label=None):
	os.system("mkdir -p preds_dir")
	i = 0
	assert len(l) == len(ids)
	while i < len(l):
		lab = None
		if label != None:
			lab = label[i]
		writeSSpreds(l[i], ids[i], seqs, lab)
		i+=1
		#raw_input()

def writeSSpreds(preds, name, seqs, label=None):
	ofp = open("preds_dir/"+name+".rnn.ss", "w")
	i = 0
	while i < len(preds):
		#print "%s\t%s\t%1.3f\t%1.3f\t%1.3f" % (seqs[name][i], US.SS_inv[U.getMaxPos(preds[i], 3)], preds[i][0], preds[i][1], preds[i][2]),
		ofp.write("%s\t%s\t%1.3f\t%1.3f\t%1.3f" % (seqs[name][i], US.SS_inv[U.getMaxPos(preds[i], 3)], preds[i][0], preds[i][1], preds[i][2]))
		if label!= None:
			#print "%s" % (US.SS_inv[label[i]])
			ofp.write("\t%s\n" % (US.SS_inv[label[i]]))
		else:
			#print
			ofp.write("\n")
		i +=1
	ofp.close()	

def buildVectors(uids, db):	#db = {uid:(seq,label)}
	X = []
	Y = []
	for u in uids:
		tmp = db[u][0]
		X.append(seq2vec(tmp))
		Y.append(db[u][1])
	return X, Y
	
def seq2vec(s):
	r = []
	listAA=['A','C','D','E','F','G','H','I','K','L','M','N','P','Q','R','S','T','V','W','Y']
	for i in s:
		tmp = np.zeros((20))	
		try:
			tmp[listAA.index(i)] = 1
		except:
			pass
		r.append(tmp.tolist())	
	return r	
	
def getEmbeddingHT(s):
	r = {}
	listAA=['A','C','D','E','F','G','H','I','K','L','M','N','P','Q','R','S','T','V','W','Y']
	for i in listAA:
		r[i] = listAA.index(i)
	print r
	return r		

if __name__ == '__main__':
	mainPRE()
	#mainEVAL()
